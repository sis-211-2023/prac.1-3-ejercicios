/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.checkstringisacronymwords;

/**
 *
 * @author Windows 10
 */
public class CheckStringIsAcronymWords {

   public boolean isAcronym(String[] words, String s) {
        if (words == null || s == null || words.length == 0 || s.length() == 0) {
            return false;
        }
        if (s.length() != words.length) {
            return false;
        }
        for (int i = 0; i < words.length; i++) {
            if (words[i].length() < 1 || words[i].charAt(0) != s.charAt(i)) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        CheckStringIsAcronymWords checker = new CheckStringIsAcronymWords();
    }