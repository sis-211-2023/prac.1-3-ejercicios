/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.findmaximumnumberstringpairs;

/**
 *
 * @author Windows 10
 */
import java.util.HashSet;
public class FindMaximumNumberStringPairs {
    public int maximumNumberOfStringPairs(String[] words) {
        if (words == null || words.length < 2 || words.length > 50) {
            return 0;
        }
        HashSet<String> reversedSet = new HashSet<>();
        int count = 0;

        for (String word : words) {
            String reversedWord = new StringBuilder(word).reverse().toString();
            if (reversedSet.contains(reversedWord)) {
                count++;
                reversedSet.remove(reversedWord);
            } else {
                reversedSet.add(word);
            }
        }
        return count;
    }
    public static void main(String[] args) {
        FindMaximumNumberStringPairs finder = new FindMaximumNumberStringPairs();
        String[] words1 = {"cd", "ac", "dc", "ca", "zz"};
        System.out.println(finder.maximumNumberOfStringPairs(words1));
        String[] words2 = {"ab", "ba", "cc"};
        System.out.println(finder.maximumNumberOfStringPairs(words2));
    }
    }
