/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.fizzbuzzz;

/**
 *
 * @author Windows 10
 */
import java.util.ArrayList;
import java.util.List;

public class FizzBuzzz {
    public List<String> fizzBuzzz(int n) {
        List<String> result = new ArrayList<>();

        for (int i = 1; i <= n; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                result.add("FizzBuzzz");
            } else if (i % 3 == 0) {
                result.add("Fizz");
            } else if (i % 5 == 0) {
                result.add("Buzzz");
            } else {
                result.add(Integer.toString(i));
            }
        }

        return result;
    }

    public static void main(String[] args) {
        FizzBuzzz fizzBuzzz = new FizzBuzzz();
        
        int n1 = 3;
        System.out.println(fizzBuzzz.fizzBuzzz(n1));  // Salida esperada: ["1", "2", "Fizz"]

        int n2 = 5;
        System.out.println(fizzBuzzz.fizzBuzzz(n2));  // Salida esperada: ["1", "2", "Fizz", "4", "Buzz"]

        int n3 = 15;
        System.out.println(fizzBuzzz.fizzBuzzz(n3));  // Salida esperada: ["1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "FizzBuzz"]
    }
}